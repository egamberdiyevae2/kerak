import React from "react";
import logo from "../images/footerLogo.svg";

const Footer = () => {
  return (
    <div className="footer">
      <div className="container">
        <div className="foot">
          <img src={logo} alt="logo" />
          <div className="foot__textsDiv">
            <ul className="foot__textsDiv__topDiv">
              <li>Home Page</li>
              <li>How it works</li>
              <li>About</li>
              <li>Contact</li>
              <li>Why us</li>
              <li>Prepaid Card</li>
              <li>Fees</li>
            </ul>
            <div className="foot__textsDiv__bottomDiv">
              <div className="leftDiv">
                <p className="botDivtext">Contact us:</p>
                <p className="botDivtext">Support@thekalibris.com</p>
              </div>
              <div className="centerDiv">
                <p className="botDivtext">Working Time:</p>
                <p className="botDivtext">
                  {" "}
                  9:00 - 23:00 GMT+3 every day, without holidays
                </p>
              </div>
              <div className="rightDiv">
                <p className="botDivtext">TheKalibris © All rights reserved </p>
                <p className="botDivtext">2015-2021</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
