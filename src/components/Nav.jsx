import React from "react";
import { Link } from "react-router-dom";
import logo from "../images//logo.svg";
import icon from "../images/icon.svg";

const Nav = () => {
  return (
    <div className="navbar">
      <div className="container">
        <div className="nav">
          <img src={logo} alt="logo" />
          <div className="nav__navRight">
            <div className="nav__navRight__Links">
              <Link className="Links__Link" to="/">
                Home Page
              </Link>
              <Link className="Links__Link" to="/why">
                Why us
              </Link>
              <Link className="Links__Link" to="/how">
                How it works
              </Link>
              <Link className="Links__Link" to="/prepaid">
                Prepaid Card
              </Link>
              <Link className="Links__Link" to="/about">
                About
              </Link>
              <Link className="Links__Link" to="/fees">
                Fees
              </Link>
              <Link className="Links__Link" to="/contact">
                Contact
              </Link>
            </div>
            <div className="nav__navRight__Buttons">
              <button className="blackButton">Log in</button>
              <button className="whiteButton">ENG</button>
            </div>
            <button className="icon">
              <img src={icon} alt="icon" />
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Nav;
