import React, { Component } from "react";

export default class Fees extends Component {
  render() {
    return (
      <div className="fees-section">
        <div className="header-item">
          <h3>Fees</h3>
          <p>
            This page informs you about the fees for using <br /> the main
            services linked to the payment <br /> account.
          </p>
        </div>
        <div className="fees-main">
          <div className="left-fee">
            <div className="leftt">
              <h5>ACCOUNT</h5>
              <div className="fee-left-item">
                <p>Account Creation</p>
                <p>FREE</p>
              </div>
              <div className="fee-left-item nott">
                <p>Account Creation</p>
                <p>FREE</p>
              </div>
              <div className="fee-left-item nott">
                <p>Account Creation</p>
                <p>FREE</p>
              </div>
              <div className="fee-left-item nott">
                <p>Account Creation</p>
                <p>FREE</p>
              </div>
              <div className="fee-left-item nott">
                <p>Account Creation</p>
                <p>FREE</p>
              </div>
            </div>
            <div className="leftt">
              <h5>WITHDRAWAL</h5>
              <div className="fee-left-item">
                <p>Account Creation</p>
                <p>FREE</p>
              </div>
              <div className="fee-left-item nott">
                <p>Account Creation</p>
                <p>FREE</p>
              </div>
            </div>
            <div className="leftt">
              <h5 style={{ paddingBottom: "0" }}>
                VIRTUAL DEBIT <br /> CARDS
              </h5>
              <div className="fee-left-item">
                <p>Account Creation</p>
                <p>FREE</p>
              </div>
              <div className="fee-left-item nott">
                <p>Account Creation</p>
                <p>FREE</p>
              </div>
              <div className="fee-left-item nott">
                <p>Account Creation</p>
                <p>FREE</p>
              </div>
              <div className="fee-left-item nott">
                <p>Account Creation</p>
                <p>FREE</p>
              </div>
              <div className="fee-left-item nott">
                <p>Account Creation</p>
                <p>FREE</p>
              </div>
            </div>
          </div>

          <div className="left-fee">
            <div className="leftt">
              <h5>DEPOSIT</h5>
              <div className="fee-left-item">
                <p>Account Creation</p>
                <p>FREE</p>
              </div>
              <div className="fee-left-item nott">
                <p>Account Creation</p>
                <p>FREE</p>
              </div>
              <div className="fee-left-item nott">
                <p>Account Creation</p>
                <p>FREE</p>
              </div>
            </div>
            <div className="leftt">
              <h5>PHYSICAL DEBIT CARDS</h5>
              <div className="fee-left-item">
                <p>Account Creation</p>
                <p>FREE</p>
              </div>
              <div className="fee-left-item nott">
                <p>Account Creation</p>
                <p>FREE</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
