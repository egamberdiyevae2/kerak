import React, { Component } from "react";

export default class Contact extends Component {
  render() {
    return (
      <div className="contact-section">
        <div className="contact-header">
          <h3>Contact us</h3>
          <p>
            Got a question? We'd love to hear from you <br /> Send us a message
            and we'll respond as sson <br /> as possible.
          </p>
        </div>
        <div className="inputs">
          <input type="text" placeholder="&#128222; Phone:" />

          <input
            type="text"
            placeholder="&#9993; E-Mail:"
            // style={{ border: "2px solid red", margin: "12px" }}
          />
          <input type="text" placeholder="👤Full Name:" />
        </div>
        <textarea placeholder="&#x1F589; Message:"></textarea>
        <button className="contact-btn">Send</button>
      </div>
    );
  }
}
